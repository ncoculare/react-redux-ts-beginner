import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { decrement, increment } from './store/counter.actions';

export const CounterPage: React.FC = () => {
  const counter = useSelector((state: RootState) => state.counter);
  const dispatch = useDispatch();

  return <div>
    Counter: {counter}

    <hr/>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(decrement(10))}>-</button>
  </div>
};
