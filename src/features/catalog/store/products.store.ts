import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Product } from '../model/product';

export const productsStore = createSlice({
  name: 'products',
  initialState: [] as Product[],
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      return action.payload
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.push(action.payload)
    },
    toggleProductVisibility(state, action: PayloadAction<Product>) {
      const product = state.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      const index = state.findIndex(p => p.id === action.payload);
      state.splice(index, 1)
    }
  }
});

export const {
  addProductSuccess, getProductsSuccess, toggleProductVisibility, deleteProductSuccess
} = productsStore.actions;


