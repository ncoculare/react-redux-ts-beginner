import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { ThunkAction } from 'redux-thunk'
import { Provider } from 'react-redux';
import { combineReducers, configureStore, Action } from '@reduxjs/toolkit';

import { Navbar } from './core/components/Navbar';
import { CounterPage } from './features/counter/CounterPage';
import { CatalogPage } from './features/catalog/CatalogPage';
import { UsersPage } from './features/users/UsersPage';
import { HomePage } from './features/home/HomePage';

import { counterReducer } from './features/counter/store/counter.reducer';
import { usersStore } from './features/users/store/users.store';
import { productsStore } from './features/catalog/store/products.store';

const rootReducer = combineReducers({
  todos: () => [],                  // useless example
  counter: counterReducer,          // simple example: actions + reducer
  users: usersStore.reducer,        // sync example: useSlice
  catalog: productsStore.reducer    // async example: useSlice / thunk
});

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>

const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
});


const App: React.FC = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />

        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>
          <Route path="/counter">
            {/*<PageCounterContainer color="red"/>*/}
            <CounterPage />
          </Route>
          <Route path="/users">
            <UsersPage />
          </Route>
          <Route path="/catalog">
            <CatalogPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
